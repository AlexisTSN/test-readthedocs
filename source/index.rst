.. CNES - HPC 6G documentation master file, created by
   sphinx-quickstart on Tue Feb 21 14:03:30 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation du pôle "Calcul Haute Performance 6G"
====================================================================

.. image:: _static/logo/centre_de_calcul_logo.png
  :width: 300
  :alt: Logo centre de calcul
  :align: center


Ce guide présente l’offre de service et les outils disponibles pour réaliser et optimiser vos travaux sur les moyens mutualisés du CNES. Pour démarrer avec les moyens de calcul, consulter les sections « Accès au cluster », « Soumission de job avec PBS » et « Environnement de travail » qui vous aideront à vous familiariser avec l'environnement mis à votre disposition.

Vous trouverez des informations sur les principaux outils et logiciels HPC dans la section « Développement et optimisation » ainsi qu'une aide à la prise en main dans la section « Fiches pratiques ». Les acronymes sont définis dans le glossaire.

Pour toute demande spécifique liée au développement d'applications de calcul intensif, adressez-vous au support HPC.

.. note::

   This project is under active development.

.. toctree::
   :hidden:

   Accueil <self>

.. toctree::
   :caption: Informations générales
   :maxdepth: 3
   :hidden:

   infos_gen/acces_cluster
   infos_gen/env_travail

.. toctree::
   :caption: Usage orienté calcul et développement
   :maxdepth: 3
   :hidden:

   calculs_dev/job_pbs
   calculs_dev/dev_opt