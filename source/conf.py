# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'CNES - HPC 6G'
copyright = '2023, CNES'
author = 'CNES'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'myst_parser'
]

templates_path = ['_templates']
exclude_patterns = []

language = 'fr'

html_logo = '_static/logo/cnes_logo.png'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_additional_pages = {'layout': 'layout.html'}

html_css_files = [
    'css/custom.css',
]

html_theme_options = {
    'style_nav_header_background': "#005191",
    'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False,
    'sidebarwidth': 300 
}

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}


html_favicon = '_static/logo/cnes_logo.jpg'

html_show_sourcelink = False
html_show_sphinx = False